usage: life.py [-h] [-cs CELL_SIZE] [-fs FULL_SCREEN] [-s SPEED]

optional arguments:
  -h, --help            show this help message and exit
  -cs CELL_SIZE, --cell-size CELL_SIZE
                        Cell size, default : 10
  -fs FULL_SCREEN, --full-screen FULL_SCREEN
                        Full screen, default : 0 (800*600)
  -s SPEED, --speed SPEED
                        Speed in miliseconds interval, default : 50