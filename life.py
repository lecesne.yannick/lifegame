from tkinter import *
import random
import argparse


def createGrid():
    x = 0
    y = 0
    #Lignes horizontales
    while x <= width:
        canvas.create_line((x, 0), (x, height), fill='black')
        x += cellSize

    #Lignes verticales
    while y <= height:
        canvas.create_line((0, y), (width, y), fill='black')
        y += cellSize


# defini une grille rempli de cellules morte
def emptyGrid():
    i = 0
    while i <= width/cellSize:
        j = 0
        while j <= height/cellSize:
            x = i*cellSize
            y = j*cellSize
            cellsGame[x, y] = 0
            j += 1
        i += 1


# defini une valeur aleatoire à toutes les cellules
def randomGrid():
    i = 0
    while i <= width/cellSize:
        j = 0
        while j <= height/cellSize:
            x = i*cellSize
            y = j*cellSize
            cellsGame[x, y] = random.randint(0, 1)
            j += 1
        i += 1


# defini si la cellule doit mourir ou vivre
def dieOrLive():

    cellsToChange = {}

    for cell in cellsGame:
        # 000
        # 0X0
        # 000

        x = cell[0]
        y = cell[1]

        liveArround = 0

        # pour chaque cellules je vérifie la vie autour

        # X00
        # 000
        # 000
        if x-cellSize > 0 and y-cellSize > 0: # delimite la premiere cellule
            if cellsGame[x-cellSize, y-cellSize] == 1:
                liveArround += 1

        # 0X0
        # 000
        # 000
        if y-cellSize > 0: # delimite la limite du haut
            if cellsGame[x, y-cellSize] == 1:
                liveArround += 1

        # 00X
        # 000
        # 000
        if x+cellSize < width and y-cellSize > 0:
            if cellsGame[x+cellSize, y-cellSize] == 1:
                liveArround += 1
        # 000
        # X00
        # 000
        if x-cellSize > 0:
            if cellsGame[x-cellSize, y] == 1:
                liveArround += 1

        # 000
        # 00X
        # 000
        if x+cellSize < width:
            if cellsGame[x+cellSize, y] == 1:
                liveArround += 1

        # 000
        # 000
        # X00
        if x-cellSize > 0 and y+cellSize < height:
            if cellsGame[x-cellSize, y+cellSize] == 1:
                liveArround += 1

        # 000
        # 000
        # 0X0
        if y+cellSize < height:
            if cellsGame[x, y+cellSize] == 1:
                liveArround += 1

        # 000
        # 000
        # 00X
        if x+cellSize < width and y+cellSize < height:
            if cellsGame[x+cellSize, y+cellSize] == 1:
                liveArround += 1

        # la cellule est morte et possède 3 voisines en vie, donc elle revie
        if liveArround == 3:
            cellsToChange[x, y] = 1

        # la cellule est vivante et ne possède pas 2 ou 3 cellules voisine, elle meurt
        if liveArround > 3 or liveArround < 2:
            cellsToChange[x, y] = 0

    return cellsToChange


# colori les cellules selon leur valeur 0 = blanc, 1 = noir
def colorGrid(cells):

    #on reset tout et recreer la grille
    canvas.delete(ALL)
    createGrid()

    for c in cells:
        x = c[0]
        y = c[1]
        if cells[c] == 1:
            canvas.create_rectangle((x, y), (x + cellSize, y + cellSize), fill='black')
        else:
            canvas.create_rectangle((x, y), (x + cellSize, y + cellSize), fill='white')


def updateWindow():

    colorGrid(cellsGame)
    cellsToChange = dieOrLive()
    for cell in cellsToChange:
        cellsGame[cell[0], cell[1]] = cellsToChange[cell[0], cell[1]]

    root.after(timeForWait, updateWindow)


def createBanshee(x, y):
    cellsGame[x, y] = 1
    cellsGame[x+cellSize, y] = 1
    cellsGame[x+cellSize*2, y] = 1
    cellsGame[x+cellSize*2, y-cellSize] = 1
    cellsGame[x+cellSize, y-cellSize*2] = 1


ap = argparse.ArgumentParser()

ap.add_argument("-cs", "--cell-size", default=10, required=False, type=int, help="Cell size, default : 10")
ap.add_argument("-fs", "--full-screen", default=0, required=False, type=int, help="Full screen, default : 0 (800*600)")
ap.add_argument("-s", "--speed", default=50, required=False, type=int, help="Speed in miliseconds interval, default : 50")

args = ap.parse_args()

cellSize = args.cell_size
fullScreen = args.full_screen
timeForWait = args.speed

#tableau des cellules avec leurs coordonnées
cellsGame = {}

width = 800
height = 600

root = Tk()

#fenêtre
if fullScreen == 1:
    root.attributes('-fullscreen', True)
    width = root.winfo_width()
    height = root.winfo_height()
else:
    root.minsize(width+cellSize, height+cellSize)
    root.maxsize(width+cellSize, height+cellSize)

canvas = Canvas(root, width=width, height=height, background="white")
canvas.pack(side=TOP, padx=cellSize, pady=cellSize)

# on cree la grille
createGrid()

# on associe des valeurs aleatoires à chaque cellules
randomGrid()

#emptyGrid()
#createBanshee(width/2, height/2)

colorGrid(cellsGame)

updateWindow()


#show window
root.mainloop()
